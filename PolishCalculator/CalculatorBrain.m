//
//  CalculatorBrain.m
//  PolishCalculator
//
//  Created by NileshAgrawal on 6/17/13.
//  Copyright (c) 2013 NileshAgrawal. All rights reserved.
//

#import "CalculatorBrain.h"

@interface CalculatorBrain ()

@property (nonatomic,strong) NSMutableArray *operandStack;
@end

@implementation CalculatorBrain

@synthesize operandStack=_operandStack;

-(NSMutableArray *)operandStack{
    
    if(!_operandStack){
        _operandStack=[[NSMutableArray alloc]init];
    }
    return _operandStack;
    
}
-(void)setOperandStack:(NSMutableArray *)anArray{
    _operandStack = anArray;
    
}
-(void)pushOperand:(double)operand{
 
    NSNumber *operandObject=[NSNumber numberWithDouble:operand];
    [self.operandStack addObject:operandObject];
    
    
}

-(double)popOperand{
    double operand = 0.0;
    NSNumber *operandObject=[self.operandStack lastObject];
    if(operandObject)[self.operandStack removeLastObject];
    operand=[operandObject doubleValue];
    
    
    return operand;
}

-(double)performOperation:(NSString *)operation{
    double result = 0.0;
    double operand2=0.0;
    if([operation isEqualToString:@"+"])
        {
            result=[self popOperand] + [self popOperand];
        }
        else if ([operation isEqualToString:@"-"])
        {
            operand2=[self popOperand];
            result=[self popOperand]-operand2;
        }
        else if ([operation isEqualToString:@"*"])
        {
            result=[self popOperand]*[self popOperand];
        }
        else if ([operation isEqualToString:@"/"])
        {
            
            operand2=[self popOperand];
            if(operand2)result = [self popOperand] / operand2;
        }
        else{
            
            //Check for the wrong operation....
            NSLog(@"Wrong Operation");
    
        }
    
    [self pushOperand:result];
    return result;
    
}
@end
