//
//  NDAAppDelegate.h
//  PolishCalculator
//
//  Created by NileshAgrawal on 6/17/13.
//  Copyright (c) 2013 NileshAgrawal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NDAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
