//
//  main.m
//  PolishCalculator
//
//  Created by NileshAgrawal on 6/17/13.
//  Copyright (c) 2013 NileshAgrawal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NDAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NDAAppDelegate class]));
    }
}
