//
//  NDAViewController.h
//  PolishCalculator
//
//  Created by NileshAgrawal on 6/17/13.
//  Copyright (c) 2013 NileshAgrawal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NDAViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *display;

- (IBAction)digitPressed:(UIButton *)sender;
-(IBAction)operationPressed:(id)sender;
-(IBAction)enterPressed;
@end
