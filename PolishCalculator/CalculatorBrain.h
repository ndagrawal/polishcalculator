//
//  CalculatorBrain.h
//  PolishCalculator
//
//  Created by NileshAgrawal on 6/17/13.
//  Copyright (c) 2013 NileshAgrawal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

-(void)pushOperand:(double)operand;
-(double)popOperand  ;

-(double)performOperation:(NSString *)operation;

@end
