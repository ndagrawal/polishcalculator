
//  NDAViewController.m
//  PolishCalculator
//
//  Created by NileshAgrawal on 6/17/13.
//  Copyright (c) 2013 NileshAgrawal. All rights reserved.
//


#import "NDAViewController.h"
#import "CalculatorBrain.h"
@interface NDAViewController ()
@property (nonatomic) BOOL userIsInMiddleOfEnteringNumber;
@property (nonatomic,strong) CalculatorBrain *brain;
@end

@implementation NDAViewController
@synthesize display;
@synthesize userIsInMiddleOfEnteringNumber;
@synthesize brain=_brain;

-(CalculatorBrain *)brain{
    if(!_brain)
        _brain=[[CalculatorBrain alloc]init];
    return _brain;
}

- (IBAction)digitPressed:(UIButton *)sender {
    
    //action to be done after the digit is pressed.
    
    NSString *digit=[sender currentTitle];
    NSLog(@"User Touched %@",digit);
   
    
    if(self.userIsInMiddleOfEnteringNumber)
    {
        self.display.text=[self.display.text stringByAppendingFormat:digit];
        
    }
    else
    {
        self.display.text=digit;
        self.userIsInMiddleOfEnteringNumber=YES;
    }
    
}

- (IBAction)operationPressed:(id)sender {
    
    if(userIsInMiddleOfEnteringNumber){
        [self enterPressed];
    }
    NSString *operation= [sender currentTitle];
    double result=[self.brain performOperation:operation];
    self.display.text=[NSString stringWithFormat:@"%g", result];
}


- (IBAction)enterPressed {
    
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.userIsInMiddleOfEnteringNumber = NO;
}




@end


